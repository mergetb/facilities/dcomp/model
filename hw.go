package dcomp

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw/components"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw/switches"
)

// Devices ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

func MinnowBoard() *hw.Device {

	return &hw.Device{
		Base: hw.Base{
			Model:        "MinnowBoard",
			Manufacturer: "ADI Engineering",
			SKU:          "MinnowBoard",
		},
		Procs: []hw.Proc{
			components.AtomE3845(),
		},
		Memory: []hw.Dimm{
			components.GenericDimm(hw.Gb(1), hw.MHz(1600), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1600), hw.DDR3),
		},
		Nics: []hw.Nic{
			components.I210AT(),
			components.I210AT(),
		},
		Ttys: []hw.Tty{
			{
				Port: &hw.Port{
					Protocols: []hw.Layer1{hw.RS232},
				},
			},
		},
		Usbs: []hw.Usb{
			{Version: hw.USB31},
			{Version: hw.USB20},
		},
	}

}

func RccVe() *hw.Device {

	return &hw.Device{
		Base: hw.Base{
			Model:        "RCC-VE",
			Manufacturer: "ADI Engineering",
			SKU:          "RCCVE",
		},
		Procs: []hw.Proc{
			components.AtomC2558(),
		},
		Memory: []hw.Dimm{
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
			components.GenericDimm(hw.Gb(1), hw.MHz(1333), hw.DDR3),
		},
		Nics: []hw.Nic{
			components.I210AT(),
			components.I210AT(),
			components.I210AT(),
			components.I210AT(),
			components.I210AT(),
			components.I210AT(),
		},
		Ttys: []hw.Tty{
			{
				Port: &hw.Port{Protocols: []hw.Layer1{hw.Uart}},
			},
		},
		Usbs: []hw.Usb{
			{Version: hw.USB20},
			{Version: hw.USB20},
		},
	}

}

func Ahi() *hw.Device {

	eno := components.I350DA2()
	eno.Kind = "eno"
	eno.StartingIndex = 1

	enp := components.ConnectX4_2x25()
	enp.Kind = "enp179s0f"

	return &hw.Device{
		Base: hw.Base{
			Model:        "DComp Ahi",
			Manufacturer: "Supermicro",
			SKU:          "DCAHI",
		},
		Procs: []hw.Proc{
			components.XeonGold6246(),
		},
		Memory: []hw.Dimm{
			components.GenericDimm(hw.Gb(32), hw.MHz(2933), hw.DDR4),
			components.GenericDimm(hw.Gb(32), hw.MHz(2933), hw.DDR4),
			components.GenericDimm(hw.Gb(32), hw.MHz(2933), hw.DDR4),
			components.GenericDimm(hw.Gb(32), hw.MHz(2933), hw.DDR4),
			components.GenericDimm(hw.Gb(32), hw.MHz(2933), hw.DDR4),
			components.GenericDimm(hw.Gb(32), hw.MHz(2933), hw.DDR4),
		},
		Nics: []hw.Nic{
			eno,
			enp,
		},
		GPUs: []hw.GPU{
			components.RTX6000(),
		},
	}

}

func XPort() *hw.Device {

	return &hw.Device{
		Base: hw.Base{
			Model:        "MinnowChassis Controller",
			Manufacturer: "Patriot Technologies",
			SKU:          "DComp100Ctl",
		},
		Nics: []hw.Nic{{
			Base: hw.Base{
				Model:        "XPort",
				Manufacturer: "Lantronix",
				SKU:          "XPORT",
				Integrated:   true,
			},
			Kind: "eth",
			Ports: []*hw.Port{{
				Protocols: []hw.Layer1{hw.Base100T},
				Capacity:  hw.Mbps(100),
			}},
		}},
	}

}

func Moa1000() *hw.Device {

	i350 := components.I350DA4()
	i350.Kind = hw.Onboard

	cx4 := components.ConnectX4_2x100()
	cx4.Kind = "enp33s0f"

	lx100 := components.NetronomeLX100()
	lx100.Kind = hw.Peripheral

	d := &hw.Device{
		Base: hw.Base{
			Model:        "Moa1000",
			Manufacturer: "Silicon Mechanics",
			SKU:          "Moa1000",
		},
		Procs: []hw.Proc{
			components.Epyc7351(),
		},
		Nics: []hw.Nic{
			i350,
			cx4,
			lx100,
			lx100,
		},
	}

	for i := 0; i < 32; i++ {
		d.Memory = append(d.Memory,
			components.GenericDimm(hw.Gb(16), hw.MHz(2667), hw.DDR4))
	}

	return d

}

func RSled1000() *hw.Device {

	i350 := components.I350DA4()
	i350.Kind = hw.Onboard

	cx4 := components.ConnectX4_2x25()
	cx4.Kind = hw.Peripheral

	d := &hw.Device{
		Base: hw.Base{
			Model:        "RSled1000",
			Manufacturer: "Silicon Mechanics",
			SKU:          "RSled1000",
		},
		Procs: []hw.Proc{
			components.Epyc7301(),
		},
		Nics: []hw.Nic{
			i350,
			cx4,
			cx4,
		},
	}

	for i := 0; i < 16; i++ {
		d.Memory = append(d.Memory,
			components.GenericDimm(hw.Gb(16), hw.MHz(2667), hw.DDR4))
	}

	return d

}

func Infrapod() *hw.Device {

	i350 := components.I350DA4()
	i350.Kind = hw.Onboard

	cx4 := components.ConnectX4_2x25()
	cx4.Kind = "enp113s0f"

	d := &hw.Device{
		Base: hw.Base{
			Model:        "DComp1000",
			Manufacturer: "Silicon Mechanics",
			SKU:          "DComp1000",
		},
		Procs: []hw.Proc{
			components.Epyc7281(),
		},
		Nics: []hw.Nic{
			i350,
			cx4,
		},
	}
	for i := 0; i < 16; i++ {
		d.Memory = append(d.Memory,
			components.GenericDimm(hw.Gb(8), hw.MHz(2667), hw.DDR4))
	}

	return d

}

func EdgecoreAS4610() *hw.Device {

	return switches.EdgeCoreAS4610()

}

func QuantaLYR4() *hw.Device {

	return switches.QuantaT1048LY4R()

}

func Msn2100() *hw.Device {

	return switches.Msn2100()

}

func Msn2700() *hw.Device {

	return switches.Msn2700()

}
