package dcomp

import (
	"encoding/binary"
	"go/build"
	"io/ioutil"
	"log"
	"net"
	"os"
	"runtime/debug"

	"github.com/mergetb/yaml/v3"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

// mac list entry
type MacEntry struct {
	Mac  string
	Name string
	Port int
}

func readMaclist(filename string) []MacEntry {

	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	var m []MacEntry
	err = yaml.Unmarshal(buf, &m)
	if err != nil {
		log.Fatal(err)
	}

	return m

}

func filepath(filename string) string {

	// try to use local first
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {

		// fall back to go module

		pkg, err := build.Default.Import(
			"gitlab.com/mergetb/facilities/dcomp/model",
			".",
			0,
		)
		if err != nil {
			log.Fatalf("could not find pakcage path: %v", err)
		}

		return pkg.Dir + "/" + filename

	}
	return filename

}

func (d *DComp) LoadMacLists() {

	maclists := []string{
		filepath("named-minnows.yml"),
		filepath("named-rohus.yml"),
	}

	for _, list := range maclists {

		m := readMaclist(list)

		for _, x := range m {
			if x.Mac == "" {
				log.Fatalf("no mac: %s", x.Name)
			}
			d.MacList[x.Name] = x.Mac
		}

	}

}

func (d *DComp) AssignMacs() {

	d.LoadMacLists()

	for _, district := range d.District {
		for _, chassis := range district.Mc {
			for _, minnow := range chassis.Minnow {

				// infranet mac
				minnow.System.Device.Nics[0].Ports[0].Mac =
					d.MacList[minnow.System.Name]

				// xpnet mac is just +1
				_, ok := d.MacList[minnow.System.Name]
				if !ok {
					continue
				}
				minnow.System.Device.Nics[1].Ports[0].Mac =
					nextMac(d.MacList[minnow.System.Name])

			}
		}
	}

	for _, district := range d.District {
		for _, rohu := range district.RccVe {

			// infranet mac
			rohu.System.Device.Nics[0].Ports[0].Mac =
				d.MacList[rohu.System.Name]

			_, ok := d.MacList[rohu.System.Name]
			if !ok {
				log.Printf("rohu %s has no macs", rohu.System.Name)
				continue
			}

			// xpnet mac is just +1,+2
			rohu.System.Device.Nics[1].Ports[0].Mac =
				nextMac(d.MacList[rohu.System.Name])

			rohu.System.Device.Nics[2].Ports[0].Mac =
				nextMac(nextMac(d.MacList[rohu.System.Name]))

		}
	}

	for _, district := range d.District {
		for _, rohu := range district.RccVe {

			// infranet mac
			rohu.System.Device.Nics[0].Ports[0].Mac =
				d.MacList[rohu.System.Name]

			_, ok := d.MacList[rohu.System.Name]
			if !ok {
				log.Printf("rohu %s has no macs", rohu.System.Name)
				continue
			}

			// xpnet mac is just +1,+2
			rohu.System.Device.Nics[1].Ports[0].Mac =
				nextMac(d.MacList[rohu.System.Name])

			rohu.System.Device.Nics[2].Ports[0].Mac =
				nextMac(nextMac(d.MacList[rohu.System.Name]))

		}
	}

	d.Ahi[0].System.Device.Nics[1].Ports[0].Mac = "b8:59:9f:bf:9c:d2"
	d.Ahi[0].System.Device.Nics[1].Ports[1].Mac = "b8:59:9f:bf:9c:d3"

	d.Ahi[1].System.Device.Nics[1].Ports[0].Mac = "b8:59:9f:bf:45:76"
	d.Ahi[1].System.Device.Nics[1].Ports[1].Mac = "b8:59:9f:bf:45:77"

	d.Ahi[2].System.Device.Nics[1].Ports[0].Mac = "b8:59:9f:b0:3b:30"
	d.Ahi[2].System.Device.Nics[1].Ports[1].Mac = "b8:59:9f:b0:3b:31"

	d.Ahi[3].System.Device.Nics[1].Ports[0].Mac = "b8:59:9f:b0:3b:1c"
	d.Ahi[3].System.Device.Nics[1].Ports[1].Mac = "b8:59:9f:b0:3b:1d"
}

func UpdateMacs(topo *xir.Net) {

	run := func(list string) {

		m := readMaclist(list)

		for _, x := range m {

			_, _, node := topo.GetNode(x.Name)
			node.Endpoints[0].Props["mac"] = x.Mac

			// xpnet mac is just +1
			node.Endpoints[1].Props["mac"] = nextMac(x.Mac)

			if len(node.Endpoints) > 2 {
				node.Endpoints[2].Props["mac"] = nextMac(nextMac(x.Mac))
			}

		}
	}

	run("named-minnows.yml")
	run("named-rohus.yml")

}

func nextMac(mac string) string {

	m, err := net.ParseMAC(mac)
	buf := append([]byte{0, 0}, []byte(m)...)
	if err != nil {
		debug.PrintStack()
		log.Fatalf("failed to parse mac '%s'", mac)
	}
	i := binary.BigEndian.Uint64(buf)
	i++
	binary.BigEndian.PutUint64(buf, i)
	m = net.HardwareAddr(buf[2:])
	return m.String()

}
