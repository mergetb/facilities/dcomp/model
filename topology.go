package dcomp

import (
	"fmt"
	"net"

	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

//TODO add infraservers
type DComp struct {
	// Emulation Servers
	Emu []*tb.Resource

	// Storage Servers
	Stor []*tb.Resource

	// Out of band management switches
	Mgmt []*tb.Resource

	// Gateway switch
	Gateway *tb.Resource

	// Experiment network spine
	XSpine *tb.Resource

	// Experiment Network fabric
	XFabric []*tb.Resource

	// Infrastructure network spine
	ISpine *tb.Resource

	// Infrastructure network switches
	IFabric []*tb.Resource

	// District subnetworks
	District []*District

	Ahi []*tb.Resource

	Infrapod []*tb.Resource

	// IP address counters
	MgIP     Ip
	MvrfIP   Ip
	IBgpIP   Ip
	XBgpIP   Ip
	MoaIP    Ip
	HarborIP Ip

	// BGP ASN counters
	IASN int
	XASN int

	// Cables
	Cables []*hw.Cable

	// Mac list
	MacList map[string]string
}

type District struct {
	// District number
	Index int

	// Parent topology
	Parent *DComp

	// Minnowchassis subnetwork
	Mc []*Chassis

	// RCC-VE nodes
	RccVe []*tb.Resource

	// Infrastructure leaves
	ILeaf []*tb.Resource

	// Experiment leaves
	XLeaf []*tb.Resource

	// Console servers
	Cs []*tb.Resource

	// Cables
	Cables []*hw.Cable
}

type Chassis struct {
	// Chassis index
	Index int

	// Parent district
	Parent *District

	// MinnowBoard nodes
	Minnow []*tb.Resource

	// Chassis power controller
	XPort *tb.Resource
}

const (
	NumEmu              = 5
	NumStor             = 4
	NumMgmt             = 4
	NumIFabric          = 4
	NumXFabric          = 5
	NumILeafPerIFabric  = 8
	NumXLeafPerXFabric  = 7
	NumILeafPerDistrict = 6
	NumXLeafPerDistrict = 7
	NumMinnowPerLeaf    = 48
	NumRccVePerLeaf     = 48
	MgmtRadix           = 48
	NumDistrict         = 5
	ChassisSize         = 24
	NumMinnowPerCs      = 96
	NumAhi              = 4
	NumInfrapod         = 2
)

func DCompTB() *DComp {

	d := &DComp{
		MgIP:     Ip{net.IPv4(172, 22, 0, 0)},
		MvrfIP:   Ip{net.IPv4(172, 22, 1, 0)},
		MoaIP:    Ip{net.IPv4(172, 22, 5, 0)},
		IBgpIP:   Ip{net.IPv4(10, 99, 0, 0)},
		XBgpIP:   Ip{net.IPv4(10, 99, 1, 0)},
		HarborIP: Ip{net.IPv4(172, 30, 0, 2)},
		MacList:  make(map[string]string),
		IASN:     64702,
		XASN:     64802,
	}

	for i := 0; i < NumMgmt; i++ {
		d.NewMgmt()
	}

	d.NewGateway()
	d.NewISpine()
	d.NewXSpine()

	for i := 0; i < NumIFabric; i++ {
		d.NewIFabric()
	}

	for i := 0; i < NumXFabric; i++ {
		d.NewXFabric()
	}

	d.IASN = 64710
	for i := 0; i < NumStor; i++ {
		d.IBgpIP = Ip{net.IPv4(10, 99, 0, 252-byte(i))}
		d.NewStor()
	}

	d.XASN = 64820
	d.XBgpIP = Ip{net.IPv4(10, 99, 1, 19)}
	for i := 0; i < NumEmu; i++ {
		d.NewEmu()
	}

	for i := 0; i < NumDistrict; i++ {
		d.NewDistrict()
	}

	for i := 0; i < NumAhi; i++ {
		d.NewAhi()
	}

	for i := 0; i < NumInfrapod; i++ {
		d.NewInfrapod()
	}

	d.AssignMacs()

	d.Connect()

	return d

}

func (d *DComp) Connect() {

	d.ConnectGateway()
	d.ConnectIFabrics()
	d.ConnectXFabrics()
	d.ConnectILeaves()
	d.ConnectXLeaves()
	d.ConnectAhis()
	d.ConnectEmus()
	d.ConnectInfrapods()

	for _, di := range d.District {
		di.ConnectMinnows()
		di.ConnectRccVes()
	}

	d.ConnectMgmt()

	d.WiringSoftFix()

}

func (d *DComp) WiringSoftFix() {

	//swap n81, n82 xpnet ports
	fmt.Printf("swapping %s with %s\n",
		d.District[1].RccVe[33].System.Name,
		d.District[1].RccVe[34].System.Name,
	)

	swapCable(
		nics(d.District[1].RccVe[33])[1].Ports[0],
		nics(d.District[1].RccVe[34])[1].Ports[0],
	)
	swapCable(
		nics(d.District[1].RccVe[33])[2].Ports[0],
		nics(d.District[1].RccVe[34])[2].Ports[0],
	)

	// swap n104, n104 xpnet ports

	fmt.Printf("swapping %s with %s\n",
		d.District[2].RccVe[8].System.Name,
		d.District[2].RccVe[9].System.Name,
	)

	swapCable(
		nics(d.District[2].RccVe[8])[1].Ports[0],
		nics(d.District[2].RccVe[9])[1].Ports[0],
	)
	swapCable(
		nics(d.District[2].RccVe[8])[2].Ports[0],
		nics(d.District[2].RccVe[9])[2].Ports[0],
	)

}

func swapCable(a, b *hw.Port) {
	ac := a.Connector
	bc := b.Connector

	a.Connector = b.Connector
	b.Connector = a.Connector

	ac.Port = b
	bc.Port = a
}

func (d *DComp) Provisional() map[string]map[string]interface{} {
	return map[string]map[string]interface{}{
		"*": {
			"image": []string{
				"debian:buster",
				"debian:10",
				"debian:11",
				"ubuntu:1804",
				"ubuntu:1810",
				"ubuntu:2004",
			},
		},
	}
}

func (d *DComp) Net() *xir.Net {

	net := xir.NewNet("dcomptb.net")

	net.AddNode(d.Gateway.Node())
	net.AddNode(d.ISpine.Node())
	net.AddNode(d.XSpine.Node())

	for _, x := range d.IFabric {
		net.AddNode(x.Node())
	}

	for _, x := range d.XFabric {
		net.AddNode(x.Node())
	}

	for _, x := range d.Mgmt {
		net.AddNode(x.Node())
	}

	for _, x := range d.Emu {
		net.AddNode(x.Node())
	}

	for _, x := range d.Stor {
		net.AddNode(x.Node())
	}

	for _, x := range d.Ahi {
		net.AddNode(x.Node())
	}

	for _, x := range d.Infrapod {
		net.AddNode(x.Node())
	}

	for _, x := range d.District {
		net.AddNet(x.Net())
	}

	for _, x := range d.Cables {
		net.AddLink(x.Link())
	}

	net.Props["provisional"] = d.Provisional()

	return net

}

func (d *District) Net() *xir.Net {

	net := xir.NewNet(fmt.Sprintf("district%d", d.Index))

	for _, x := range d.RccVe {
		net.AddNode(x.Node())
	}

	for _, x := range d.ILeaf {
		net.AddNode(x.Node())
	}

	for _, x := range d.XLeaf {
		net.AddNode(x.Node())
	}

	for _, x := range d.Cs {
		net.AddNode(x.Node())
	}

	for _, mc := range d.Mc {
		net.AddNet(mc.Net())
	}

	for _, x := range d.Cables {
		net.AddLink(x.Link())
	}

	return net

}

func (c *Chassis) Net() *xir.Net {

	net := xir.NewNet(fmt.Sprintf("chassis%d", c.Index))

	net.AddNode(c.XPort.Node())

	for _, x := range c.Minnow {

		node := x.Node()
		net.AddNode(node)

	}

	return net

}
