package dcomp

import (
	"fmt"
	"log"

	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func check(topo *xir.Net) {

	checkMinnowNbr(topo)
	checkRccNbr(topo)
	checkIleafNbr(topo)
	checkXleafNbr(topo)
	checkIfabricNbr(topo)
	checkXfabricNbr(topo)
	checkMinnowConsoleNbr(topo)

}

func checkMinnowNbr(topo *xir.Net) {

	for i := 0; i < 1200; i++ {

		district := i / 240
		ileaf := district*6 + (i%240)/48
		xleaf := district*7 + (i%240)/48

		name := fmt.Sprintf("m%d", i)
		_, _, m := topo.GetNode(name)
		if m == nil {
			log.Fatalf("minnow %s not found", name)
		}

		var ileafFound, xleafFound bool
		for _, n := range m.Neighbors() {

			if tb.HasRole(n.Parent, tb.Leaf) &&
				tb.HasRole(n.Parent, tb.InfraSwitch) &&
				n.Parent.Label() == fmt.Sprintf("il%d", ileaf) {
				ileafFound = true
			}

			if tb.HasRole(n.Parent, tb.Leaf) &&
				tb.HasRole(n.Parent, tb.XpSwitch) &&
				n.Parent.Label() == fmt.Sprintf("xl%d", xleaf) {
				xleafFound = true
			}

		}

		if !ileafFound {
			log.Fatalf("no ileaf for %s", name)
		}
		if !xleafFound {
			log.Fatalf("no xleaf for %s", name)
		}

	}

}

func checkRccNbr(topo *xir.Net) {

	for i := 0; i < 240; i++ {

		district := i / 48
		ileaf := district*6 + 5
		xleaf0 := district*7 + 5
		xleaf1 := district*7 + 6

		name := fmt.Sprintf("n%d", i)
		_, _, rcc := topo.GetNode(name)
		if rcc == nil {
			log.Fatalf("rccve %s not found", name)
		}

		var ileafn, xleafn int
		for _, n := range rcc.Neighbors() {

			if tb.HasRole(n.Parent, tb.Leaf) &&
				tb.HasRole(n.Parent, tb.InfraSwitch) &&
				n.Parent.Label() == fmt.Sprintf("il%d", ileaf) {
				ileafn += 1
			}

			if tb.HasRole(n.Parent, tb.Leaf) &&
				tb.HasRole(n.Parent, tb.XpSwitch) &&
				n.Parent.Label() == fmt.Sprintf("xl%d", xleaf0) {
				xleafn += 1
			}

			if tb.HasRole(n.Parent, tb.Leaf) &&
				tb.HasRole(n.Parent, tb.XpSwitch) &&
				n.Parent.Label() == fmt.Sprintf("xl%d", xleaf1) {
				xleafn += 1
			}

		}

		if ileafn != 1 {
			log.Fatalf("unexpected ileaf count %d != 1 for %s", ileafn, name)
		}
		if xleafn != 2 {
			log.Fatalf("unexpected xleaf count %d != 2 for %s", xleafn, name)
		}

	}
}

func checkIleafNbr(topo *xir.Net) {

	for i := 0; i < 30; i++ {

		name := fmt.Sprintf("il%d", i)
		_, _, leaf := topo.GetNode(name)
		if leaf == nil {
			log.Fatalf("leaf %s not found", name)
		}

		fabric := i / 8
		var fabricFound bool
		for _, n := range leaf.Neighbors() {

			if tb.HasRole(n.Parent, tb.Fabric) &&
				tb.HasRole(n.Parent, tb.InfraSwitch) &&
				n.Parent.Label() == fmt.Sprintf("if%d", fabric) {
				fabricFound = true
				break
			}

		}
		if !fabricFound {
			log.Fatalf("ileaf %s has no fabric neighbor", name)
		}

	}

}

func checkXleafNbr(topo *xir.Net) {

	for i := 0; i < 35; i++ {

		name := fmt.Sprintf("xl%d", i)
		_, _, leaf := topo.GetNode(name)
		if leaf == nil {
			log.Fatalf("leaf %s not found", name)
		}

		fabric := i / 7
		var fabricFound bool
		for _, e := range leaf.Endpoints {
			for _, nbr := range e.Neighbors {

				n := nbr.Endpoint
				if tb.HasRole(n.Parent, tb.Fabric) &&
					tb.HasRole(n.Parent, tb.XpSwitch) &&
					n.Parent.Label() == fmt.Sprintf("xf%d", fabric) {

					// downlink
					linkinfo := sys.LinkInfo(e)
					if linkinfo == nil {
						log.Fatalf("expected linkinfo for fabric uplink on %s", name)
					}

					if linkinfo.Parent.Name != "uplink" {
						log.Fatalf("expected uplink on bond for leaf %s", name)
					}

					// uplink
					linkinfo = sys.LinkInfo(n)
					if linkinfo == nil {
						log.Fatalf("expected linkinfo for fabric downlink on %s", name)
					}

					if linkinfo.Parent.Name != name {
						log.Fatalf("expected downlink with leaf name on bond for leaf %s != %s",
							linkinfo.Parent.Name,
							name,
						)
					}

					fabricFound = true
					break
				}

			}
		}
		if !fabricFound {
			log.Fatalf("xleaf %s has no fabric neighbor", name)
		}

	}

}

func checkIfabricNbr(topo *xir.Net) {

	for i := 0; i < 4; i++ {

		name := fmt.Sprintf("if%d", i)
		_, _, fabric := topo.GetNode(name)
		if fabric == nil {
			log.Fatalf("fabric %s not found", name)
		}

		var spineFound bool
		for _, n := range fabric.Neighbors() {

			if tb.HasRole(n.Parent, tb.Spine) &&
				tb.HasRole(n.Parent, tb.InfraSwitch) &&
				n.Parent.Label() == "isp0" {
				spineFound = true
				break
			}

		}

		if !spineFound {
			log.Fatalf("ifabric %s has no spine neighbor", name)
		}

	}

}

func checkXfabricNbr(topo *xir.Net) {

	for i := 0; i < 5; i++ {

		name := fmt.Sprintf("xf%d", i)
		_, _, fabric := topo.GetNode(name)
		if fabric == nil {
			log.Fatalf("fabric %s not found", name)
		}

		var spineFound bool
		for _, n := range fabric.Neighbors() {

			if tb.HasRole(n.Parent, tb.Spine) &&
				tb.HasRole(n.Parent, tb.XpSwitch) &&
				n.Parent.Label() == "xsp0" {
				spineFound = true
				break
			}

		}

		if !spineFound {
			log.Fatalf("xfabric %s has no spine neighbor", name)
		}

	}

}

func checkMinnowConsoleNbr(topo *xir.Net) {

	for i := 0; i < 1200; i++ {

		name := fmt.Sprintf("m%d", i)
		_, _, m := topo.GetNode(name)
		if m == nil {
			log.Fatalf("minnow %s not found", name)
		}

		var consolePort *xir.Endpoint
		for _, n := range m.Neighbors() {

			if tb.HasRole(n.Parent, tb.ConsoleServer) {
				consolePort = n
				break
			}

		}
		if consolePort == nil {
			log.Fatalf("no console server for %s", name)
		}

		log.Printf("%s ~~~> %s::%s",
			name,
			consolePort.Parent.Label(),
			consolePort.Props["name"].(string),
		)

	}

}
