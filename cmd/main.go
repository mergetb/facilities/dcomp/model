package main

import (
	"gitlab.com/mergetb/facilities/dcomp/model"
	"gitlab.com/mergetb/xir/lang/go/v0.2/build"
)

func main() {

	tb := dcomp.DCompTB()
	build.Run(tb.Net())

}
