package dcomp

import (
	"fmt"

	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw/cables"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw/devices"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

const (
	minnowAppend = "root=PARTUUID=a0000000-0000-0000-0000-00000000000a " +
		"rootfstype=ext4 " +
		"console=tty1 console=ttyS0,115200n8 console=tty1 " +
		"console=ttyS0,115200n8 earlyprintk=ttyS0 " +
		"rw " +
		"net.ifnames=0 biosdevname=0 " +
		"8250.nr_uarts=1 " +
		"infranet=eth0"

	rohuAppend = "root=PARTUUID=a0000000-0000-0000-0000-00000000000a " +
		"rootfstype=ext4 " +
		"console=ttyS1,115200n8 " +
		"rw " +
		"net.ifnames=0 biosdevname=0 " +
		"infranet=eth0"

	ahiAppend = "root=PARTUUID=a0000000-0000-0000-0000-00000000000a " +
		"rootfstype=ext4 " +
		"console=ttyS1,115200n8 " +
		"rw " +
		"infranet=enp179s0f0"

	defaultImage = "debian-10-disk"
)

func (d *DComp) LoadMacList() {

}

func (d *DComp) NewEmu() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("emu%d", len(d.Emu)),
		Moa1000(),
	)
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.MoaIP.Next().IP().String())
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: d.XBgpIP.Next().IP().String(),
			ASN:      d.XASN,
		},
	}

	d.XASN++

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NetEmuAlloc},
		Roles:  []tb.Role{tb.NetworkEmulator},
		LinkRoles: map[string]tb.Role{
			"eno0":      tb.MgmtLink,
			"enp33s0f0": tb.EmuLink,
		},
	}

	d.Emu = append(d.Emu, resource)
	return resource

}

func (d *DComp) NewStor() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("stor%d", len(d.Stor)),
		RSled1000(),
	)
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.MgIP.Next().IP().String())
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: d.IBgpIP.Next().IP().String(),
			ASN:      d.IASN,
		},
	}

	system.OS.Links = append(system.OS.Links, &sys.Link{
		Name:  "sledbr",
		Kind:  "bridge",
		Addrs: []string{d.HarborIP.Next().IP().String()},
	})

	d.IASN++

	resource := &tb.Resource{
		System: system,
		Alloc: []tb.AllocMode{
			tb.FilesystemAlloc,
			tb.BlockDeviceAlloc,
		},
		Roles: []tb.Role{
			tb.StorageServer,
			tb.SledHost,
		},
		LinkRoles: map[string]tb.Role{
			"eno0":   tb.MgmtLink,
			"enp1":   tb.InfraLink,
			"sledbr": tb.HarborEndpoint,
		},
	}

	d.Stor = append(d.Stor, resource)
	return resource

}

func (d *DComp) NewMgmt() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("mg%d", len(d.Mgmt)),
		QuantaLYR4(),
	)
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.MgIP.Next().IP().String())

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles:  []tb.Role{tb.MgmtSwitch},
		LinkRoles: map[string]tb.Role{
			"eno0": tb.MgmtLink,
		},
	}

	d.Mgmt = append(d.Mgmt, resource)
	return resource

}

func (d *DComp) NewGateway() *tb.Resource {

	system := sys.NewSystem("gateway", Msn2100())
	system.OS.Link(sys.Gbps(1))[0].AddAddr("172.22.0.254")
	system.OS.Link(sys.Lo())[0].AddAddr("10.99.0.1")
	system.OS.Config = &sys.OsConfig{
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles:  []tb.Role{tb.Gateway},
		Props:  xir.Props{"name": "gateway"},
	}

	d.Gateway = resource
	return resource

}

func (d *DComp) NewISpine() *tb.Resource {

	system := sys.NewSystem("isp0", Msn2100())
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.MvrfIP.Next().IP().String())
	system.OS.Link(sys.Lo())[0].AddAddr(d.IBgpIP.Next().IP().String())
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: d.IBgpIP.Next().IP().String(),
			ASN:      d.IASN,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	d.IASN++

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.InfraSwitch,
			tb.Spine,
		},
	}

	d.ISpine = resource
	return resource

}

func (d *DComp) NewXSpine() *tb.Resource {

	system := sys.NewSystem("xsp0", Msn2700())
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.MvrfIP.Next().IP().String())
	system.OS.Link(sys.Lo())[0].AddAddr(d.XBgpIP.Next().IP().String())
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: d.XBgpIP.Next().IP().String(),
			ASN:      d.XASN,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	d.XASN++

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NetAlloc},
		Roles: []tb.Role{
			tb.XpSwitch,
			tb.Spine,
		},
		LinkRoles: make(map[string]tb.Role),
	}

	labelXpSwp(resource)

	d.XSpine = resource
	return resource

}

func labelXpSwp(resource *tb.Resource) {

	for _, n := range resource.System.Device.Nics {
		if n.Kind == "swp" {
			for i, _ := range n.Ports {
				name := fmt.Sprintf("%s%d", n.Kind, n.StartingIndex+i)
				resource.LinkRoles[name] = tb.XpLink
			}
		}
	}

}

func (d *DComp) NewIFabric() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("if%d", len(d.IFabric)),
		Msn2100(),
	)
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: d.IBgpIP.Next().IP().String(),
			ASN:      d.IASN,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	d.IASN++

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.InfraSwitch,
			tb.Fabric,
		},
	}

	d.IFabric = append(d.IFabric, resource)
	return resource

}

func (d *DComp) NewXFabric() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("xf%d", len(d.XFabric)),
		Msn2100(),
	)
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: d.XBgpIP.Next().IP().String(),
			ASN:      d.XASN,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	d.XASN++

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NetAlloc},
		Roles: []tb.Role{
			tb.XpSwitch,
			tb.Fabric,
		},
		LinkRoles: make(map[string]tb.Role),
		Props:     xir.Props{},
	}

	labelXpSwp(resource)

	d.XFabric = append(d.XFabric, resource)
	return resource

}

const (
	NumDistrictRccVe = 48
	NumDistrictILeaf = 6
	NumDistrictXLeaf = 7
	NumDistrictCS    = 3
	NumDistrictMC    = 10
)

func (d *DComp) NewDistrict() *District {

	district := &District{
		Index:  len(d.District),
		Parent: d,
	}

	for i := 0; i < NumDistrictRccVe; i++ {
		district.NewRccVe()
	}

	for i := 0; i < NumDistrictILeaf; i++ {
		district.NewILeaf()
	}

	for i := 0; i < NumDistrictXLeaf; i++ {
		district.NewXLeaf()
	}

	for i := 0; i < NumDistrictCS; i++ {
		district.NewConsoleServer()
	}

	for i := 0; i < NumDistrictMC; i++ {
		district.NewChassis()
	}

	d.District = append(d.District, district)

	return district

}

func (d *District) NewRccVe() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("n%d", d.Index*NumDistrictRccVe+len(d.RccVe)),
		RccVe(),
	)

	system.OS.Config = &sys.OsConfig{
		Append:  rohuAppend,
		Rootdev: "sda",
	}

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		LinkRoles: map[string]tb.Role{
			"eth0": tb.InfraLink,
			"eth1": tb.XpLink,
			"eth2": tb.XpLink,
		},
		DefaultImage: defaultImage,
		ImageVariant: "rohu",
		Props: xir.Props{
			"memory": hw.Gb(8),
		},
		Ptags: []string{"*"},
	}

	d.RccVe = append(d.RccVe, resource)
	return resource

}

func (d *DComp) NewAhi() *tb.Resource {

	system := sys.NewSystem(fmt.Sprintf("a%d", len(d.Ahi)), Ahi())

	system.OS.Config = &sys.OsConfig{
		Append:  ahiAppend,
		Rootdev: "sda",
	}

	r := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		LinkRoles: map[string]tb.Role{
			"enp179s0f0": tb.InfraLink,
			"enp179s0f1": tb.XpLink,
		},
		DefaultImage: defaultImage,
		ImageVariant: "rohu",
		//XXX hack
		Props: xir.Props{
			"memory": hw.Gb(192),
			"gpu":    "RTX6000",
		},
		Ptags: []string{"*"},
	}

	d.Ahi = append(d.Ahi, r)
	return r
}

func (d *DComp) NewInfrapod() *tb.Resource {

	system := sys.NewSystem(fmt.Sprintf("site%d", len(d.Infrapod)), Infrapod())

	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: "10.99.0.254",
			ASN:      64701,
		},
	}

	r := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles:  []tb.Role{tb.InfrapodServer},
		LinkRoles: map[string]tb.Role{
			"enp113s0f0": tb.InfraLink,
			"enp113s0f1": tb.GatewayLink, //TODO in reality it's enp113s0f1.11
		},
	}

	d.Infrapod = append(d.Infrapod, r)
	return r

}

func (d *District) NewILeaf() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("il%d", d.Index*NumDistrictILeaf+len(d.ILeaf)),
		EdgecoreAS4610(),
	)
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.Parent.MvrfIP.Next().IP().String())
	bgpip := d.Parent.IBgpIP.Next().IP()
	system.OS.Link(sys.Lo())[0].AddAddr(bgpip.String())
	system.OS.Config = &sys.OsConfig{
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.InfraSwitch,
			tb.Leaf,
		},
	}

	d.ILeaf = append(d.ILeaf, resource)
	return resource

}

func (d *District) NewXLeaf() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("xl%d", d.Index*NumDistrictXLeaf+len(d.XLeaf)),
		EdgecoreAS4610(),
	)
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.Parent.MvrfIP.Next().IP().String())
	system.OS.Link(sys.Lo())[0].AddAddr(d.Parent.XBgpIP.Next().IP().String())
	system.OS.Config = &sys.OsConfig{
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NetAlloc},
		Roles: []tb.Role{
			tb.XpSwitch,
			tb.Leaf,
		},
		LinkRoles: make(map[string]tb.Role),
	}

	labelXpSwp(resource)

	d.XLeaf = append(d.XLeaf, resource)
	return resource

}

func (d *District) NewConsoleServer() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("cs%d", d.Index*NumDistrictCS+len(d.Cs)),
		devices.Zpe(96),
	)
	system.OS.Link(sys.Gbps(1))[0].AddAddr(d.Parent.MgIP.Next().IP().String())

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles:  []tb.Role{tb.ConsoleServer},
	}

	d.Cs = append(d.Cs, resource)
	return resource

}

func (d *District) NewChassis() *Chassis {

	c := &Chassis{
		Index:  d.Index*NumDistrictMC + len(d.Mc),
		Parent: d,
	}

	c.NewXPort()

	for i := 0; i < NumChassisMinnow; i++ {
		c.NewMinnow()
	}

	d.Mc = append(d.Mc, c)

	return c

}

const (
	NumChassisMinnow = 24
	NumCsPerDistrict = 3
)

func (c *Chassis) NewXPort() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("mc%d", c.Index),
		XPort(),
	)
	system.OS.Links[0].AddAddr(c.Parent.Parent.MgIP.Next().IP().String())

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.NoAlloc},
		Roles:  []tb.Role{tb.PowerController},
	}

	c.XPort = resource
	return resource

}

func (c *Chassis) NewMinnow() *tb.Resource {

	system := sys.NewSystem(
		fmt.Sprintf("m%d", c.Index*NumChassisMinnow+len(c.Minnow)),
		MinnowBoard(),
	)

	system.OS.Config = &sys.OsConfig{
		Append:  minnowAppend,
		Rootdev: "sda",
	}

	resource := &tb.Resource{
		System: system,
		Alloc:  []tb.AllocMode{tb.PhysicalAlloc},
		Roles:  []tb.Role{tb.Node},
		LinkRoles: map[string]tb.Role{
			"eth0": tb.InfraLink,
			"eth1": tb.XpLink,
		},
		DefaultImage: defaultImage,
		ImageVariant: "minnow",
		Props: xir.Props{
			"memory": hw.Gb(2),
		},
		Ptags: []string{"*"},
	}

	c.Minnow = append(c.Minnow, resource)
	return resource

}

// Connection methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (d *DComp) ConnectGateway() {

	cable := d.Cable(cables.Fs100GDAC())
	cable.Connectors[0][0].Connect(nics(d.Gateway)[1].Ports[9])
	cable.Connectors[1][0].Connect(nics(d.XSpine)[1].Ports[15])

	cable = d.Cable(cables.Fs100GDAC())
	cable.Connectors[0][0].Connect(nics(d.Gateway)[1].Ports[10])
	cable.Connectors[1][0].Connect(nics(d.ISpine)[1].Ports[8])

}

func (d *DComp) ConnectIFabrics() {

	p := 0
	for _, f := range d.IFabric {

		for i := 0; i < 3; i++ {
			cable := d.Cable(cables.Fs100GDAC())
			cable.Connectors[0][0].Connect(nics(d.ISpine)[1].Ports[p])
			cable.Connectors[1][0].Connect(nics(f)[1].Ports[13+i])
			p += 1
		}
		// jump from swp6 to swp11
		if p == 6 {
			p = 10
		}

	}

}

func (d *DComp) ConnectXFabrics() {

	p := 0
	for _, f := range d.XFabric[:2] {

		for i := 0; i < 3; i++ {
			cable := d.Cable(cables.Fs100GDAC())
			cable.Connectors[0][0].Connect(nics(d.XSpine)[1].Ports[p])
			cable.Connectors[1][0].Connect(nics(f)[1].Ports[13+i])
			p += 1
		}

		f.System.OS.Bond("uplink", f.System.OS.Link(sys.Gbps(100))[13:16]...)
		d.XSpine.System.OS.Bond(f.System.Name, d.XSpine.System.OS.Link(sys.Gbps(100))[p-3:p]...)

	}

	p = 31
	for _, f := range d.XFabric[2:] {

		for i := 0; i < 3; i++ {
			cable := d.Cable(cables.Fs100GDAC())
			cable.Connectors[0][0].Connect(nics(d.XSpine)[1].Ports[p])
			cable.Connectors[1][0].Connect(nics(f)[1].Ports[13+i])
			p -= 1
		}

		f.System.OS.Bond("uplink", f.System.OS.Link(sys.Gbps(100))[13:16]...)
		d.XSpine.System.OS.Bond(f.System.Name, d.XSpine.System.OS.Link(sys.Gbps(100))[p-3:p]...)

	}

}

func (d *DComp) ConnectILeaves() {

	i := 0
	L := NumILeafPerIFabric
	for _, di := range d.District {
		for _, l := range di.ILeaf {

			f := d.IFabric[i/L]
			fp := nics(f)[1].Ports[i%L]

			ports := nics(l)[2].Ports
			cable := d.Cable(cables.FsMpoBreakout40x10())
			cable.Connectors[0][0].Connect(fp)
			cable.Connectors[1][0].Connect(ports[0])
			cable.Connectors[1][1].Connect(ports[1])
			cable.Connectors[1][2].Connect(ports[2])
			cable.Connectors[1][3].Connect(ports[3])

			l.System.OS.Bond("uplink", l.System.OS.Link(sys.Gbps(10))...)
			f.System.OS.Bond(l.System.Name, f.System.OS.Link(sys.Gbps(100))[i%L])

			i += 1

		}
	}

}

func (d *DComp) ConnectXLeaves() {

	i := 0
	L := NumXLeafPerXFabric
	for _, di := range d.District {
		for _, l := range di.XLeaf {

			f := d.XFabric[i/L]
			fp := nics(f)[1].Ports[i%L]

			ports := nics(l)[2].Ports
			cable := d.Cable(cables.FsMpoBreakout40x10())
			cable.Connectors[0][0].Connect(fp)
			cable.Connectors[1][0].Connect(ports[0])
			cable.Connectors[1][1].Connect(ports[1])
			cable.Connectors[1][2].Connect(ports[2])
			cable.Connectors[1][3].Connect(ports[3])

			l.System.OS.Bond("uplink", l.System.OS.Link(sys.Gbps(10))...)
			f.System.OS.Bond(l.System.Name, f.System.OS.Link(sys.Gbps(100))[i%L])

			i += 1

		}
	}

}

func (d *DComp) ConnectAhis() {

	// infranet

	is := d.ISpine
	isp := nics(is)[1].Ports[6]

	icable := d.Cable(cables.FsDACBreakout100x25())
	icable.Connectors[0][0].Connect(isp)

	// xpnet

	xs := d.XSpine
	xsp := nics(xs)[1].Ports[18]

	xcable := d.Cable(cables.FsDACBreakout100x25())
	xcable.Connectors[0][0].Connect(xsp)

	for i, x := range d.Ahi {

		icable.Connectors[1][i].Connect(nics(x)[1].Ports[0])
		nics(x)[1].Ports[0].Breakout = fmt.Sprintf("swp7s%d", i)

		xcable.Connectors[1][i].Connect(nics(x)[1].Ports[1])
		nics(x)[1].Ports[1].Breakout = fmt.Sprintf("swp19s%d", i)

	}

}

func (d *DComp) ConnectEmus() {

	xs := d.XSpine
	ports := []*hw.Port{
		nics(xs)[1].Ports[6],  //swp7
		nics(xs)[1].Ports[8],  //swp9
		nics(xs)[1].Ports[10], //swp11
		nics(xs)[1].Ports[12], //swp13
		nics(xs)[1].Ports[14], //swp15
	}

	for i, x := range d.Emu {

		cable := d.Cable(cables.Fs100GDAC())
		cable.Connectors[0][0].Connect(ports[i])
		cable.Connectors[1][0].Connect(nics(x)[1].Ports[0])

	}

}

func (d *DComp) ConnectInfrapods() {

	gw := d.Gateway
	gwp := nics(gw)[1].Ports[15]

	cable := d.Cable(cables.FsDACBreakout100x25())
	cable.Connectors[0][0].Connect(gwp)

	for i, x := range d.Infrapod {

		cable.Connectors[1][i].Connect(nics(x)[1].Ports[0])
		nics(x)[1].Ports[0].Breakout = fmt.Sprintf("swp16s%d", i)

	}

}

func (di *District) ConnectMinnows() {

	i := 0
	M := NumMinnowPerLeaf
	C := NumMinnowPerCs
	for c, mc := range di.Mc {
		for _, m := range mc.Minnow {

			cable := di.Cable(cables.Cat6())
			cable.Connectors[0][0].Connect(nics(di.ILeaf[i/M])[1].Ports[i%M])
			cable.Connectors[1][0].Connect(nics(m)[0].Ports[0])

			cable = di.Cable(cables.Cat6())
			cable.Connectors[0][0].Connect(nics(di.XLeaf[i/M])[1].Ports[i%M])
			cable.Connectors[1][0].Connect(nics(m)[1].Ports[0])

			// minnows in 2nd district rac got wired after rcc-ves
			j := i
			if c >= 8 {
				j += 48
			}

			cable = di.Cable(cables.Cat6())
			cable.Connectors[0][0].Connect(di.Cs[j/C].System.Device.Ttys[j%C].Port)
			cable.Connectors[1][0].Connect(m.System.Device.Ttys[0].Port)

			i += 1

		}
	}

}

func (di *District) ConnectRccVes() {

	i := 0
	for _, rcc := range di.RccVe {

		cable := di.Cable(cables.Cat6())
		cable.Connectors[0][0].Connect(nics(di.ILeaf[5])[1].Ports[i])
		cable.Connectors[1][0].Connect(nics(rcc)[0].Ports[0])

		cable = di.Cable(cables.Cat6())
		cable.Connectors[0][0].Connect(nics(di.XLeaf[5])[1].Ports[i])
		cable.Connectors[1][0].Connect(nics(rcc)[1].Ports[0])

		cable = di.Cable(cables.Cat6())
		cable.Connectors[0][0].Connect(nics(di.XLeaf[6])[1].Ports[i])
		cable.Connectors[1][0].Connect(nics(rcc)[2].Ports[0])

		i += 1

	}

}

func (d *DComp) ConnectMgmt() {

	i := 0
	N := MgmtRadix

	connectMgmt := func(r *tb.Resource) {
		cable := d.Cable(cables.Cat6())

		cable.Connectors[0][0].Connect(nics(r)[0].Ports[0])
		cable.Connectors[1][0].Connect(nics(d.Mgmt[i/N])[1].Ports[i%N])

		i += 1
	}

	connectMgmt(d.ISpine)
	connectMgmt(d.XSpine)

	for _, x := range d.IFabric {
		connectMgmt(x)
	}

	for _, x := range d.XFabric {
		connectMgmt(x)
	}

	for _, di := range d.District {

		for _, x := range di.ILeaf {
			connectMgmt(x)
		}

		for _, x := range di.XLeaf {
			connectMgmt(x)
		}

		for _, x := range di.Cs {
			connectMgmt(x)
		}

	}

}

func (d *DComp) Cable(c *hw.Cable) *hw.Cable {
	d.Cables = append(d.Cables, c)
	return c
}

func (d *District) Cable(c *hw.Cable) *hw.Cable {
	d.Cables = append(d.Cables, c)
	return c
}
